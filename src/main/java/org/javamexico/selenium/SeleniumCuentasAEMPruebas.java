package org.javamexico.selenium;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumCuentasAEMPruebas {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/Users/carlos/Proyectos/Husares/EjemplosSelenium/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://www.bbva.pe/personas/prueba-cuentas.html");		
		List<WebElement> iframeElements = driver.findElements(By.tagName("iframe"));
		System.out.println("Total numero de iframe " + iframeElements.size());
		
		driver.switchTo().frame("content-iframe");
		driver.switchTo().frame("iframeCuentasReact");
		
		driver.findElement(By.xpath("//input[@name='txtNumeroDni']")).sendKeys("41188590");
		driver.findElement(By.xpath("//input[@name='txtCelular']")).sendKeys("991395234");
		driver.findElement(By.xpath("//input[@name='txtCorreo']")).sendKeys("carssdas@www.com");
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div[2]/div/div[3]/button")).click();
		
		Thread.sleep(360000);
		driver.quit();
	}

}
